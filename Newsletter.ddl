CREATE TABLE dynamic (
  id           int(11) NOT NULL AUTO_INCREMENT, 
  dynamic_ukey varchar(20), 
  ukey         varchar(20) NOT NULL, 
  name         varchar(255) NOT NULL, 
  `order`      decimal(5, 2) DEFAULT 0 NOT NULL, 
  status       tinyint(1) DEFAULT 1 NOT NULL, 
  dynamic_fk   varchar(50), 
  value        varchar(255), 
  PRIMARY KEY (id)) type=InnoDB;
