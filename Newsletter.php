<?php

namespace Model;

/**
 * Dynamic
 *
 * Concept of a newsletter storage
 *
 * @package     Newsletter
 * @category	
 * @author	Bruno Foggia
 * @link	
 */
class Newsletter {

    use \doctrine\Dashes\Model {
        create as protected _create;
    }

    protected $table = 'newsletter';
    protected $foreignKeys = [
    ];
    protected $fieldsFormat = [
        'phone' => ['/\D+/', ''],
        'created' => ':',
    ];

    public function create($data) {
        $existent = $this->getBy(['email'=>$data['email']], [$this->getAttr('primaryKey')]);
        if(!empty($existent)) {
            return $this->update($existent[$this->getAttr('primaryKey')], $data);
        }
        return $this->_create($data);
    }

}
